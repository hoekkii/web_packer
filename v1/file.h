inline s32
DirectoryUp(string s, s32 amount)
{
	s32 i = s.Length - 1;
	while (amount > 0)
	{
		s32 x = 0;
		while (i >= 0)
		{
			if (s[i] == '/' || s[i] == '\\')
			{
				if (x) x = 2;
			}
			else if (x == 2) break;
			else x = 1;

			i--;
		}

		amount--;
	}

	return i >= 0 ? i + 1 : s.Length;
}

inline s32
DirectoryUp(string s)
{
	s32 x = 0;
	s32 i = s.Length - 1;
	while (i >= 0)
	{
		if (s[i] == '/' || s[i] == '\\')
		{
			if (x) x = 2;
		}
		else if (x == 2) return i;
		else x = 1;

		i--;
	}

	return s.Length;
}



inline string
ReadAllText(const char* path)
{
	var file = fopen(path, "rb");
	if (!file)
	{
		return {
			nullptr, 0, 0
		};
	}

	fseek(file, 0, SEEK_END);
	s32 size = ftell(file);
	fseek(file, 0, SEEK_SET);

	auto buffer = malloc(size + 1);
	fread(buffer, 1, size, file);

	string result;
	result.VData = buffer;
	result.Data[size] = 0;
	result.Length = size;
	result.Capacity = size + 1;
	fclose(file);
	return result;
}
