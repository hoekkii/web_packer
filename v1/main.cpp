#define _CRT_SECURE_NO_DEPRECATE
#define WEB_PACKER_INTERNAL 1
#define WEB_PACKER_DEBUG 1

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <Windows.h>
#include <tchar.h>
#include "types.h"
#include "string.h"
#include "file.h"
#include "node.h"
#include "parser.h"




void Parse(string& path, string& content, string& output)
{
	var previous = '\0';
	for (var i = 0; i < content.Length; i++)
	{
		var c0 = content[i];
		if (i < content.Length - 5 && c0 == '{' && content[i + 1] == '{')
		{
			i++;
			var start = i + 1;
			for (var j = start; j < content.Length - 1; j++)
			{
				if (content[j] == '}' && content[j + 1] == '}')
				{
					var file = Substring(content, start, j - start);
					var other = String(path);
					Append(other, file);
					
					var otherContent = ReadAllText(other.CData);
					if (otherContent.CData == nullptr)
					{
						print("Cannot find file (%s)", other.CData);
						delete[] file.Data;
						delete[] other.Data;
						return;
					}
					
					// Deep parse when it's the appropiate file type
					const auto test = &file.CData[file.Length - 5];
					if (Equals(".html", test, 5) ||
						Equals(".vert", test, 5) ||
						Equals(".frag", test, 5) ||
						Equals(".frag", test, 5) ||
						Equals(".css", &file.CData[file.Length - 4], 4) ||
						Equals(".js", &file.CData[file.Length - 3], 3))
						Parse(path, otherContent, output);
					else
						Append(output, otherContent);
					
					delete[] file.Data;
					delete[] other.Data;
					delete[] otherContent.Data;
					i = j + 1;
					break;
				}
			}
		}
		else if (c0 != '\r')
		{
			if (output.Length + 2 >= output.Capacity)
				Grow(output, output.Capacity * 2);
			
			if (c0 == '\n' && i > 0 && previous == '\n')
				continue;
				
			Append(output, c0);
			previous = c0;
		}
	}
}


void Rebuild(string basepath, const string inpath, const string outpath)
{
	var content = ReadAllText(inpath.CData);
	var parsed = String();
	Grow(parsed, content.Capacity * 4);
	Parse(basepath, content, parsed);


	s32 i = 0;
	node n;
	n.tag = String();
	n.attributes = nullptr;
	n.attribute_count = 0;
	n.children = nullptr;
	n.child_count = 0;
	n.content = { nullptr,0,0 };
	n.single = 0;
	n.text = 0;
	context ct{ parsed, i };
	Foo(ct, &n);

	var s = String(1024);
	Append(s, "<!DOCTYPE html>\n");
	ToString(n, s, 0);


	var output = (s);
	var file = fopen(outpath.CData, "w");
	fprintf(file, "%s", output.CData);
	fclose(file);
	delete[] output.Data;
	delete[] content.Data;
}

int main(s32 argc, const char* args[])
{
	assert(sizeof(char) == 1);
	
	string exe_name;
	string folder;
	string full_path = String(args[0]);
	for (int i = full_path.Length - 1; i >= 0; --i)
	{
		if (full_path[i] == '.')
		{
			for (int j = i - 1; j >= 0; --j)
			{
				if (full_path[j] == '\\' || full_path[j] == '/')
				{
					folder = Substring(full_path, 0, j + 1);
					exe_name = Substring(full_path, j + 1, i - j - 1);
					break;
				}
			}
			break;
		}
	}
	
	string base_path{ nullptr, 0, 0 };
	string* in_paths;
	string* out_paths;
	s32 paths_length;
	var settingsPath = folder + "web_packer.ini";
	var settingsContent = ReadAllText(settingsPath.CData);
	if (settingsContent.CData)
	{
		paths_length = 0;
		in_paths = new string[10];
		out_paths = new string[10];
		
		print("Settings file found, parsing file..\n");
		string p0;
		string p1;
		for (var i = 0; i < settingsContent.Length; i++)
		{
			var sc = settingsContent[i];
			if (sc == ' ' || sc == '\n' || sc == '\t' || sc == '\r')
				continue;
			
			if (base_path.CData)
			{
				
				var j = i;
				while (i < settingsContent.Length)
				{
					var c = settingsContent[i];
					if (c == '|')
						break;
					
					i++;
				}
				
				var in_relative = Substring(settingsContent, j, i - j);
				in_paths[paths_length] = String(folder);
				Append(in_paths[paths_length], in_relative);
				Free(in_relative);
				
				
				i++;
				j = i;
				while (i < settingsContent.Length)
				{
					var c = settingsContent[i];
					if (c == '\n' || c == '\r')
						break;
					
					i++;
				}
				var out_relative = Substring(settingsContent, j, i - j);
				out_paths[paths_length] = String(folder);
				Append(out_paths[paths_length], out_relative);
				Free(out_relative);
				
				paths_length++;
			}
			else
			{
				var j = i;
				while (i < settingsContent.Length)
				{
					var c = settingsContent[i];
					if (c == '\n' || c == '\r')
						break;
					
					i++;
				}
				
				var base_relative = Substring(settingsContent, j, i - j);
				base_path = String(folder);
				Append(base_path, base_relative);
				Free(base_relative);
			}
		}
	}
	else
	{
		print("No settings file found, using default values\n");
		base_path = String(folder);
		Append(base_path, "Source");
		
		in_paths = new string;
		in_paths->Length = folder.Length;
		in_paths->Capacity = folder.Capacity + 19;
		in_paths->Data = reinterpret_cast<uchar*>(malloc((s64)folder.Capacity + 19L));
		memcpy(in_paths->Data, folder.Data, folder.Capacity);
		Append(*in_paths, "Source/index.html");
		
		out_paths = new string;
		out_paths->Length = folder.Length;
		out_paths->Capacity = folder.Capacity + 26;
		out_paths->Data = reinterpret_cast<uchar*>(malloc((s64)folder.Capacity + 26L));
		memcpy(out_paths->Data, folder.Data, folder.Capacity);
		Append(*out_paths, "WebApp/public/index.html");
		
		paths_length = 1;
	}
	
	
	
	print("EXE:  %s\n", exe_name.CData);
	print("PATH: %s\n", folder.CData);
	print("VARS: %s\n", settingsPath.CData);
	print("BASE: %s\n", base_path.CData);
	if (paths_length < 1)
	{
		print("No paths found, exiting program..");
		return 0;
	}
	
	print("First parse:\n");
	for (var j = 0; j < paths_length; j++)
	{
		Rebuild(base_path, in_paths[j], out_paths[j]);
		print("%s > %s\n", in_paths[j].CData, out_paths[j].CData);
	}
	print("\n");
	
	_CrtDumpMemoryLeaks();
	
	const auto handles = FindFirstChangeNotification(
		base_path.CData,
		TRUE,
		FILE_NOTIFY_CHANGE_FILE_NAME |
		FILE_NOTIFY_CHANGE_DIR_NAME |
		FILE_NOTIFY_CHANGE_ATTRIBUTES |
		FILE_NOTIFY_CHANGE_SIZE |
		FILE_NOTIFY_CHANGE_LAST_WRITE |
		FILE_NOTIFY_CHANGE_LAST_ACCESS |
		FILE_NOTIFY_CHANGE_CREATION |
		FILE_NOTIFY_CHANGE_SECURITY);
	
	if (handles == INVALID_HANDLE_VALUE)
	{
		print("ERROR: FindFirstChangeNotification function failed.\n");
		ExitProcess(GetLastError());
	}
	
	for (var i = 0;; i++)
	{
		print("\nWaiting for change..\n");
		switch (WaitForSingleObject(handles, INFINITE))
		{
			case WAIT_OBJECT_0:
			{
				print("Change detected\n");
				for (var j = 0; j < paths_length; j++)
				{
					Rebuild(base_path, in_paths[j], out_paths[j]);
					print("%s > %s\n", in_paths[j].CData, out_paths[j].CData);
				}
				
				_CrtDumpMemoryLeaks();
				
				if (FindNextChangeNotification(handles) == FALSE)
				{
					print("ERROR: FindFirstChangeNotification function failed.\n");
					ExitProcess(GetLastError());
				}
			} break;
			
			case WAIT_TIMEOUT:
			{
				print("\nNo changes in the timeout period.\n");
			} break;
			
			default:
			{
				print("ERROR: Unhandles status.\n");
				ExitProcess(GetLastError());
			}
		}
	}
	
	return 0;
}
