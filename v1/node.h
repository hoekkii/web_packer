
string* ValueOf(node* node, const char* key)
{
	for (var j = 0; j < node->attribute_count; j++)
	{
		var& attribute = node->attributes[j];
		if (Equals(attribute.type, key))
			return &attribute.value;
	}

	return nullptr;
}

inline void
AppendChild(node* parent, node* child)
{
	// TODO: Convert to memory pool and copy the memory once the node is completed
	if (parent->children == nullptr)
	{
		parent->children = new node * [1];
		parent->children[0] = child;
		parent->child_count = 1;
	}
	else
	{
		parent->child_count++;
		parent->children = reinterpret_cast<node * *>(realloc(parent->children, parent->child_count * sizeof(node*)));
		parent->children[parent->child_count - 1] = child;
	}
}

inline void
AppendAttribute(node* n, attribute a)
{
	if (n->attributes)
	{
		n->attribute_count++;
		n->attributes = reinterpret_cast<attribute*>(realloc(n->attributes, n->attribute_count * sizeof(attribute)));
		n->attributes[n->attribute_count - 1] = a;
	}
	else
	{
		n->attributes = new attribute[1];
		n->attributes[0] = a;
		n->attribute_count = 1;
	}
}
