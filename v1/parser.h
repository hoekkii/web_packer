
inline bool
SelfClosing(string s)
{
	return Equals(s, "area")
		|| Equals(s, "base")
		|| Equals(s, "br")
		|| Equals(s, "col")
		|| Equals(s, "embed")
		|| Equals(s, "hr")
		|| Equals(s, "img")
		|| Equals(s, "input")
		|| Equals(s, "link")
		|| Equals(s, "meta")
		|| Equals(s, "param")
		|| Equals(s, "source")
		|| Equals(s, "track")
		|| Equals(s, "wbr");
}



#define MoveNext ++ct.i<ct.html.Length
#define Current ct.html[ct.i]
inline void
SkipToNextToken(context& ct)
{
	do {
		if (!IsWhiteSpace(Current))
			return;
	} while (MoveNext);
}

inline void
PrintError(context& i, const char* error)
{
	print(error);
}







void ParsePlaneScript(context& ct, node* n1)
{
	// Treat this as plain text
	// < / script>
	var start = ct.i;
	while (MoveNext)
	{
		var end = ct.i;
		if (Current != '<')
			continue;

		// Skip reading til we have a closing tag
		ct.i++;
		SkipToNextToken(ct);
		if (Current != '/')
			continue;
		ct.i++;
		SkipToNextToken(ct);

		// Read script tag
		var token = ct.html.CData + ct.i;
		if (Equals(token, "script", 6))
		{
			ct.i += 6;
			SkipToNextToken(ct);
			if (Current != '>')
				continue;

			var script = new node;
			script->tag = { nullptr,0,0 };
			script->attributes = nullptr;
			script->attribute_count = 0;
			script->children = nullptr;
			script->child_count = 0;
			script->content = Substring(ct.html, start, end - start);
			script->single = 0;
			script->text = 1;
			script->css = 0;
			AppendChild(n1, script);
			return;
		}
	}
}

void ParseJavaScript(context& ct, node* n1)
{
	// TODO: Low priority, parse JavaScript
	ParsePlaneScript(ct, n1);
}


/*
* {
	margin: 0;
	padding: 0;
}

body {
	background: black;
	color: white;
}

div > a
{
	color: blue;
}
*/
attribute ParseSingleCSS(context& ct)
{
	attribute result;
	SkipToNextToken(ct);
	var start = ct.i;
	var end = start;
	while (MoveNext)
	{
		if (Current == ':')
		{
			ct.i++;
			SkipToNextToken(ct);
			var valueStart = ct.i;
			var valueEnd = valueStart;

			while (MoveNext)
			{
				if (Current == ';')
				{
					ct.i++;
					result = {
						Substring(ct.html, start, end - start + 1),
						Substring(ct.html, valueStart, valueEnd - valueStart + 1),
					};
					return result;
				}
				else if (!IsWhiteSpace(Current))
				{
					valueEnd = ct.i;
				}
			}
		}
		else if (!IsWhiteSpace(Current))
		{
			end = ct.i;
		}
	}

}


void ParseCSS(context& ct, node* n1)
{
	do
	{
		SkipToNextToken(ct);
		var start = ct.i;
		var end = start;
		do
		{
			if (Current == '{')
			{
				ct.i++;
				var n = new node;
				n->tag = Substring(ct.html, start, end - start + 1);
				n->attributes = nullptr;
				n->attribute_count = 0;
				n->children = nullptr;
				n->child_count = 0;
				n->content = { nullptr, 0,0 };
				n->single = 0;
				n->text = 0;
				n->css = 1;

				SkipToNextToken(ct);
				while (Current != '}')
				{
					var attr = ParseSingleCSS(ct);
					AppendAttribute(n, attr);
					SkipToNextToken(ct);
				}
				ct.i++;

				AppendChild(n1, n);
				ParseCSS(ct, n1);
				return;
			}
			else if (Current == '<')
			{
				while (MoveNext && Current != '>');
				ct.i++;
				return;
			}
			else if (!IsWhiteSpace(Current))
			{
				end = ct.i;
			}
		} while (MoveNext);
	} while (MoveNext);
}





void ParseAttribute(context& ct, string token, node* n)
{
	if (n->tag.Data == nullptr)
	{
		n->tag = token;
		return;
	}

	SkipToNextToken(ct);

	var c = Current;
	if (c != '=')
	{
		AppendAttribute(n, { token, nullptr, 0, 0 });
		return;
	}

	ct.i++;
	SkipToNextToken(ct);
	c = Current;

	var value = String();
	if (c == '"')
	{
		bool escaping = false;
		Append(value, '"');
		while (MoveNext)
		{
			c = Current;
			switch (c)
			{
				case '"':
				{
					Append(value, '"');
					if (!escaping)
					{
						// Finished parsing the attribute
						AppendAttribute(n, { token, value });
						return;
					}
				} break;

				case '\\':
				{
					Append(value, '\\');
					escaping = true;
				} break;

				case '\n':
				case '\r':
				case '\t':
				{
					PrintError(ct, "string not closed!");
				} return;

				default:
				{
					Append(value, c);
					escaping = false;
				} break;
			}
		}
	}
	else
	{
		do
		{
			c = Current;
			if (IsWhiteSpace(c) || c == '>')
			{
				// Finished parsing the attribute
				AppendAttribute(n, { token, value });
				break;
			}

			Append(value, c);
		} while (MoveNext);
	}
}

node* ParseElementStart(context& ct)
{
	SkipToNextToken(ct);

	var n = new node;
	n->tag = { nullptr,0,0 };
	n->attributes = nullptr;
	n->attribute_count = 0;
	n->children = nullptr;
	n->child_count = 0;
	n->content = { nullptr,0,0 };
	n->single = 0;
	n->text = 0;
	n->css = 0;

	var content = String();

	do {
		var c = Current;
		switch (c)
		{
			case ' ':
			case '\t':
			case '\n':
			case '\r':
			case '=': {
				if (content.Length < 1)
					continue;

				ParseAttribute(ct, content, n);
				content = String();
			} continue;

			case '/': {
				// TODO: After this we expect > but should we catch errors when one does not do that?
				n->single = 1;
				ct.i++;
			} continue;

			case '>': {
				ParseAttribute(ct, content, n);
				content = String();

				if (SelfClosing(n->tag))
					n->single = 1;
				ct.i++;
				return n;
			} break;
		}

		Append(content, Current);
	} while (MoveNext);

	// ERROR: n->tag did not close
	return n;
}

void Foo(context& ct, node* parent)
{
	bool was_whitespace = true;
	string content = String();
	do {
		var c = Current;
		var is_whitespace = IsWhiteSpace(c);
		if (was_whitespace && is_whitespace)
			continue;

		if (is_whitespace)
		{
			Append(content, ' ');
			was_whitespace = true;
			continue;
		}

		if (c == '<')
		{
			if (content.Length > 0)
			{
				if (was_whitespace)
				{
					content.Length--;
					content[content.Length] = '\0';
				}

				var n = new node;
				n->tag = { nullptr,0,0 };
				n->attributes = nullptr;
				n->attribute_count = 0;
				n->children = nullptr;
				n->child_count = 0;
				n->content = content;
				n->single = 0;
				n->text = 1;
				n->css = 0;
				AppendChild(parent, n);
				content = String();
			}

			while (MoveNext)
			{
				c = Current;
				if (IsWhiteSpace(c))
					continue;

				if (c == '/')
				{
					// TODO: Closing thing
					Free(content);
					while (ct.html[++ct.i] != '>');
					return;
				}

				var n1 = ParseElementStart(ct);
				AppendChild(parent, n1);

				if (Equals(n1->tag, "script"))
				{
					var type = ValueOf(n1, "type");
					if (type == nullptr || Equals(*type, "application/javascript"))
					{
						ParseJavaScript(ct, n1);
						break;
					}
					else if (Equals(*type, "\"text/html\""))
					{
						// Do parse this normally
					}
					else
					{
						ParsePlaneScript(ct, n1);
						break;
					}

					// TODO: Check if parsing text/html is necessary

					// TODO: check for type
				}
				else if (Equals(n1->tag, "style"))
				{
					ParseCSS(ct, n1);
					break;
				}

				if (!n1->single)
					Foo(ct, n1);

				break;
			}
		}
		else
		{
			was_whitespace = false;
			Append(content, c);
		}
	} while (MoveNext);
	Free(content);
}

void FixWebkit(attribute a, string& s, s32 tab)
{
	#define WebkitEquals(str) Equals(a.type, str)
	#define WebkitAppend(str) for (var k=0;k<tab;k++)Append(s,'\t');Append(s,str);Append(s,": ");Append(s,a.value);Append(s,";\n");
	WebkitAppend(a.type);

	if (WebkitEquals("margin"))
	{
		WebkitAppend("margin-left");
	}
}


void ToString(node n, string& s, s32 tab)
{
	for (var i = 0; i < n.child_count; i++)
	{
		var child = *n.children[i];
		for (var j = 0; j < tab; j++)
			Append(s, '\t');

		if (child.text)
		{
			Append(s, child.content);
			Append(s, '\n');
		}
		else if (child.css)
		{
			Append(s, child.tag);
			Append(s, " {\n");
			for (var j = 0; j < child.attribute_count; j++)
				FixWebkit(child.attributes[j], s, tab + 1);

			for (var k = 0; k < tab; k++)
				Append(s, '\t');

			Append(s, " }\n");
		}
		else
		{
			Append(s, '<');
			Append(s, child.tag);

			for (var j = 0; j < child.attribute_count; j++)
			{
				var attribute = child.attributes[j];
				Append(s, ' ');
				Append(s, attribute.type);
				if (attribute.value.Data)
				{
					Append(s, '=');
					Append(s, attribute.value);
				}
			}


			if ((child.tag.CData) && Equals(child.tag.CData, "script", 6))
			{
				Append(s, ">");
				for (var j = 0; j < child.child_count; j++)
					Append(s, child.children[j]->content);
				Append(s, "</");
				Append(s, child.tag);
				Append(s, ">\n");
			}
			else if (child.single)
			{
				Append(s, "/>\n");
			}
			else
			{
				if (child.child_count > 0) // SHouldn't this be child?
				{
					Append(s, ">\n");
					ToString(child, s, tab + 1);
					for (var i = 0; i < tab; i++)
						Append(s, '\t');
					Append(s, "</");
					Append(s, child.tag);
					Append(s, ">\n");
				}
				else
				{
					Append(s, "></");
					Append(s, child.tag);
					Append(s, ">\n");
				}
			}
		}
	}
}

