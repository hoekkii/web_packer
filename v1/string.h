

inline string
String()
{
	string result;
	result.Length = 0;
	result.Data = reinterpret_cast<uchar*>(malloc(8));
	result.Data[0] = '\0';
	result.Capacity = 8;

	return result;
}

inline string
String(s32 capacity)
{
	string result;
	result.Length = 0;
	result.Data = reinterpret_cast<uchar*>(malloc(capacity));
	result.Data[0] = '\0';
	result.Capacity = capacity;

	return result;
}

inline string
String(string s)
{
	string result;
	result.Length = s.Length;
	result.Capacity = s.Capacity;
	result.Data = reinterpret_cast<uchar*>(malloc(s.Capacity));
	memcpy(result.Data, s.Data, result.Capacity);
	return result;
}

inline string
String(const char* s)
{
	assert(s);

	string result;
	result.Length = strlen(s);
	result.Capacity = result.Length + 1;
	result.Data = reinterpret_cast<uchar*>(malloc(result.Capacity));
	memcpy(result.Data, s, result.Capacity);
	return result;
}

inline const string
CString(const char* s)
{
	string result;
	result.Length = strlen(s);
	result.Capacity = result.Length + 1;
	result.CData = s;
	return result;
}

inline void
Free(string& s)
{
	if (s.Capacity < 1)
		return;

	free(s.Data);
	s.Length = 0;
	s.Capacity = 0;
}

inline s32
Hash(const char* s)
{
	u32 result = 0;
	while (*s)
		result = 101 * result + *s++;

	return result;
}

inline s32
Hash(string& s)
{
	s32 result = 0;
	for (s32 i = 0; i < s.Length; i++)
		result = 101 * result + s[i];

	return result;
}




inline bool
Equals(string l, string r)
{
	return l.Length == r.Length
		&& Equals(l.Data, r.Data, r.Length);
}

inline bool
Equals(string l, string r, s32 length)
{
	return Equals(l.Data, r.Data, length);
}

inline bool
Equals(string l, const char* r)
{
	for (s32 i = 0; i < l.Length; i++)
	{
		if (*reinterpret_cast<const uchar*>(r++) != l[i])
			return false;
	}

	return *r == '\0';
}

inline bool
Equals(string l, const char* r, s32 length)
{
	for (s32 i = 0; i < length; i++)
	{
		if (*reinterpret_cast<const uchar*>(r++) != l[i])
			return false;
	}

	return true;
}




// TODO: Should we account for weird accents stuff?
inline bool
IsLower(uchar c) { return c >= 'a' && c <= 'z'; }

inline uchar
ToLower(uchar c) { return c >= 'A' && c <= 'Z' ? c + ('a' - 'A') : c; }

inline bool
IsUpper(uchar c) { return c >= 'A' && c <= 'Z'; }

inline uchar
ToUpper(uchar c) { return c >= 'a' && c >= 'z' ? c - ('a' - 'A') : c; }

inline string&
Upper(string& s)
{
	for (s32 i = 0; i < s.Length; i++)
	{
		uchar& c = s.Data[i];
		if (IsLower(c))
			c = c - ('a' - 'A');
	}

	return s;
}

inline string&
Lower(string& s)
{
	for (s32 i = 0; i < s.Length; i++)
	{
		uchar& c = s.Data[i];
		if (IsUpper(c))
			c = c + ('a' - 'A');
	}

	return s;
}


inline string&
Shorten(string& s, s32 length)
{
	assert(length < s.Length);

	s.Length = length;
	s.Data[s.Length + 1] = '\0';
	return s;
}

inline string&
Grow(string& s, s32 length)
{
	if (s.Capacity >= length)
		return s;

	s.Data = reinterpret_cast<uchar*>(realloc(s.Data, length));
	s.Capacity = length;
	return s;
}


inline string&
Append(string& l, const char* r, s32 length)
{
	if (!length || !r)
		return l;

	s32 capacity = length + 1;
	s32 total = capacity + l.Length;

	if (l.Capacity >= total)
	{
		memcpy(l.Data + l.Length, r, capacity);
		l.Length += length;
		l.Data[l.Length] = '\0';
		return l;
	}

	l.Data = reinterpret_cast<uchar*>(realloc(l.Data, total));
	memcpy(l.Data + l.Length, r, capacity);
	l.Length = total - 1;
	l.Capacity = total;
	l.Data[l.Length] = '\0';
	return l;
}




inline string&
Append(string& l, char r)
{
	s32 total = l.Length + 2;
	if (l.Capacity >= total)
	{
		l.Data[l.Length++] = r;
		l.Data[l.Length] = '\0';
		return l;
	}

	l.Data = reinterpret_cast<uchar*>(realloc(l.Data, total));
	l.Data[l.Length++] = r;
	l.Data[l.Length] = '\0';
	l.Capacity = total;
	return l;
}


inline string&
Append(string& l, const char* r) { return Append(l, r, strlen(r)); }

inline string&
Append(string& l, string& r) { return Append(l, r.CData, r.Length); }

inline string
Substring(string& s, s32 start, s32 length)
{
	assert(start >= 0);
	assert(start <= s.Length);
	assert(length >= 0);
	assert(start <= s.Length - length);

	string result;
	result.Length = length;
	result.Capacity = length;

	if (length)
	{
		result.Data = reinterpret_cast<uchar*>(malloc(length + 1));
		memcpy(result.Data, s.Data + start, length);
		result.Data[length] = '\0';
	}

	return result;
}

inline string
Substring(string& s, s32 start) { return Substring(s, start, s.Length - start); }

inline bool
IsEmpty(string& s) { return s.Length <= 0; }

inline string&
operator += (string& l, const char* r) { return Append(l, r); }

inline string&
operator += (string& l, string& r) { return Append(l, r); }

inline bool
operator == (string& l, string& r) { return Equals(l, r); }

inline bool
operator == (string& l, const char* r) { return Equals(l, r); }

inline bool
operator == (const char* l, string& r) { return Equals(r, l); }


inline string
operator + (string l, string r)
{
	string result;
	result.Length = l.Length + r.Length;
	result.Capacity = result.Length + 1;
	result.Data = reinterpret_cast<uchar*>(malloc(result.Capacity));
	memcpy(result.Data, l.Data, l.Length);
	memcpy(result.Data + l.Length, r.Data, r.Length);
	result.Data[result.Length] = '\0';
	return result;
}


inline string
operator + (string l, const char* r)
{
	var rlength = strlen(r);
	string result;
	result.Length = l.Length + rlength;
	result.Capacity = result.Length + 1;
	result.Data = reinterpret_cast<uchar*>(malloc(result.Capacity));
	memcpy(result.Data, l.Data, l.Length);
	memcpy(result.Data + l.Length, r, rlength);
	result.Data[result.Length] = '\0';
	return result;
}


inline bool
IsWhiteSpace(uchar c)
{
	return c == ' '
		|| c == '\t'
		|| c == '\n'
		|| c == '\r';
}
