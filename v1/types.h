// Base types
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int32_t b32;
typedef float r32;
typedef double r64;
typedef intptr_t intptr;
typedef uintptr_t uintptr;
typedef unsigned char uchar;

// Defines
#define null nullptr
#define var auto
#define let auto

// Constants
#define PI 3.141592653589793f
#define TAU 6.283185307179586f
#define DEG2RAD 0.017453292519943295f
#define RAD2DEG 57.29577951308232f
#define EPSILON 0.00001f
#define S8MIN  -128
#define S16MIN -32768
#define S32MIN -2147483648
#define S64MIN -9223372036854775808L
#define S8MAX  127
#define S16MAX 32767
#define S32MAX 2147483647
#define S64MAX 9223372036854775807L
#define U8MAX  255
#define U16MAX 65535
#define U32MAX 4294967295U
#define U64MAX 18446744073709551615UL

// assert and s(tatic)_assert
#define CRASH *(int*)0=0
#if WEB_PACKER_DEBUG
#define assert(expr) if (!(expr)) { fprintf(stderr, "FAILED assertion [" __FILE__ ":%i] " #expr "\n", __LINE__);  CRASH; }
#define s_assert(expr) static_assert(expr, "FAILED assertion: " #expr)
#else
#define assert(expr)
#define s_assert(expr)
#endif

#if WEB_PACKER_INTERNAL
#define NotImplemented assert(!"NotImplemented")
#else
#define NotImplemented NotImplemented!
#endif

#define print(...) fprintf(stderr, __VA_ARGS__)
#define PPCAT_NX(A, B) A ## B
#define PPCAT(A, B) PPCAT_NX(A, B)

#define ARGUMENT_AT_0(arg0, ...) arg0
#define ARGUMENT_AT_1(arg0, arg1, ...) arg1
#define ARGUMENT_AT_2(arg0, arg1, arg2, ...) arg2
#define ARGUMENT_AT_3(arg0, arg1, arg2, arg3, ...) arg3
#define ARGUMENT_AT_4(arg0, arg1, arg2, arg3, arg4, ...) arg4

#define For_0(arr, i) for (s32 i = 0; i < arr.Length; ++i)
#define For_1(arr) for (s32 i = 0; i < arr.Length; ++i)
#define For(...) ARGUMENT_AT_2(__VA_ARGS__, For_0, For_1)(__VA_ARGS__)

// Forr(eversed)
#define Forr_0(arr, i) for (s32 i = arr.Length - 1; i >= 0; --i)
#define Forr_1(arr) for (s32 i = arr.Length - 1; i >= 0; --i)
#define Forr(...) ARGUMENT_AT_2(__VA_ARGS__, Forr_0, Forr_1)(__VA_ARGS__)


// Complex types
struct buffer
{
	u8* Data;
	s32 Length;
    
	inline u8& operator [] (s32 i)
	{
		assert(i >= 0);
		assert(i < this->Length);
		return this->Data[i];
	}
};

struct varbuffer : public buffer
{
	s32 Capacity;
};

struct string
{
	union
	{
		uchar* Data;
		u8* UData;
		char* SData;
		const char* CData;
		void* VData;
	};
    
	s32 Length;
	s32 Capacity;
    
	inline uchar& operator [] (s32 i)
	{
		assert(i >= 0);
		assert(i < this->Capacity);
		return this->Data[i];
	}
};


struct attribute
{
	string type;
	string value;
};

struct node
{
    string tag;
	attribute* attributes;
    s32 attribute_count;
    node** children;
    s32 child_count;
    string content;
	b32 single;
    b32 text;
	b32 css;
};

struct context
{
	string html;
	s32 i;
	// TODO: Add buffers here
};


















// https://sites.google.com/site/murmurhash/
inline s32
MurmurHash2(const uchar* data, s32 len, u32 seed = 0)
{
	const s32 m = 0x5BD1E995;
	const s32 r = 24;
	u32 h = seed ^ len;
	while (len >= 4)
	{
		s32 k = *reinterpret_cast<const s32*>(data);
		k *= m;
		k ^= k >> r;
		k *= m;
		h *= m;
		h ^= k;

		data += 4;
		len -= 4;
	}

	switch (len)
	{
	case 3: h ^= data[2] << 16;
	case 2: h ^= data[1] << 8;
	case 1: h ^= data[0];
		h *= m;
	}

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}

inline s32
HashOf(const void* key, s32 len)
{
	return MurmurHash2(reinterpret_cast<const uchar*>(key), len, 0);
}

inline bool
Equals(const void* l, const void* r, s32 length)
{
	const uchar* L = reinterpret_cast<const uchar*>(l);
	const uchar* R = reinterpret_cast<const uchar*>(r);
	while (length >= 4)
	{
		if (*reinterpret_cast<const u32*>(L) != *reinterpret_cast<const u32*>(R))
			return false;

		L += 4;
		R += 4;
		length -= 4;
	}

	switch (length)
	{
	case 3: if (L[2] != R[2]) return false;
	case 2: if (L[1] != R[1]) return false;
	case 1: if (L[0] != R[0]) return false;
	}

	return true;
}






















