#define ARGUMENTS_HELP "help:\n"\
		"usage: web_packer.exe -i <input filename> <output filename>"\
		"-i    <input filename> <output filename> The input file is relative to the working directory: this is the file that will be parsed.\n"\
		"-w    <directory>                        Directory that will be watched for changes"\
		"-r    <directory>                        The root directory is the path when a path starts with /. This is relative to the executable directory. If not defines it will be the same directory as the executable\n"\
		"-o    <output option>                    How the syntax tree is output. options include: [minify: no indenting, no comments] [beautify: indenting, no comments] [debug: indenting, comments]"\
		"-h                                       Print program usage and parameters\n"\

program_arguments RetrieveProgramArguments(s32 argc, const char* args[], static_buffer_pool* pool)
{
	program_arguments result;
	result.root_directory = { nullptr, 0, 0 };
	result.flags = program_arguments::NONE;

	var watches_buffer = StaticBuffer(pool);
	var ptr_watches_buffer = reinterpret_cast<uchar*>(watches_buffer->data);
	var watches = StaticBuffer(pool);
	var str_watches = reinterpret_cast<string*>(watches->data);
	var watches_count = 0;
	var io_bufferr = StaticBuffer(pool);
	var ptr_io_buffer = reinterpret_cast<uchar*>(io_bufferr->data);
	var ios = StaticBuffer(pool);
	var str_ios = reinterpret_cast<string*>(ios->data);
	var ios_count = 0;

	var tmp_buffer = StaticBuffer(pool);
	var exe_path = String(args[0], tmp_buffer);
	Terminate(exe_path);
	CleanupPath(exe_path);
	result.exe_path = StringAlloc(exe_path);
	result.exe_directory = StringAlloc(DirectoryOf(result.exe_path));

	// TODO: be able to use multiple input files
	for (var i = 1; i < argc; i++)
	{
		if (Equals(args[i], "-i"))
		{
			if (i + 2 >= argc)
				crash("-i requires two arguments. Exiting..");

			// Input file
			{
				i++;
				var arg = args[i];
				var length = strlen(arg);
				memcpy(ptr_io_buffer, arg, length);
				string input_file = {
					ptr_io_buffer,
					length,
					length,
				};
				ptr_io_buffer += length;
				str_ios[ios_count] = input_file;
				ios_count++;
			}

			// Output
			{
				i++;
				var arg = args[i];
				var length = strlen(arg);
				memcpy(ptr_io_buffer, arg, length);
				string output_file = {
					ptr_io_buffer,
					length,
					length,
				};
				ptr_io_buffer += length;
				str_ios[ios_count] = output_file;
				ios_count++;
			}
		}
		else if (Equals(args[i], "-r"))
		{
			if (result.root_directory.data)
				crash("Working directory already setup. Exiting..");

			if (i + 1 >= argc)
				crash("-w requires one argument. Exiting..");

			var root = String(args[++i], tmp_buffer);
			Terminate(root);
			CleanupPath(root);
			result.root_directory = StringAlloc(root);
		}
		else if (Equals(args[i], "-w"))
		{
			if (i + 1 >= argc)
				crash("-i requires two arguments. Exiting..");

			// Input file
			i++;
			var arg = args[i];
			var length = strlen(arg);
			memcpy(ptr_watches_buffer, arg, length);
			string directory = {
				ptr_watches_buffer,
				length,
				length,
			};
			ptr_watches_buffer += length;
			str_watches[watches_count] = directory;
			watches_count++;
		}
		else if (Equals(args[i], "-o"))
		{
			if (result.flags)
				crash("Flags already setup. Exiting..");

			if (i + 1 >= argc)
				crash("-o requires one argument. Exiting..");

			i++;
			if (Equals(args[i], "minify"))
				result.flags = program_arguments::MINIFY_OUTPUT;
			else if (Equals(args[i], "beautify"))
				result.flags = program_arguments::BEAUTIFY_OUTPUT;
			else if (Equals(args[i], "debug"))
				result.flags = program_arguments::DEBUG_OUTPUT;
		}
		else if (Equals(args[i], "-h") || Equals(args[i], "--help"))
		{
			print(ARGUMENTS_HELP);
			exit(0);
		}
		else
		{
			crash("Invalid argument: " CC_UNDERLINE "%s\n\n" ARGUMENTS_HELP, args[i]);
		}
	}


	if (!result.root_directory.data)
		result.root_directory = result.exe_directory;

	if (ios_count)
	{
		assert(!(ios_count & 1));
		result.io_count = ios_count / 2;
		result.io = alloc(input_output, result.io_count);
		for (var i = 0; i < result.io_count; i++)
		{
			var j = i * 2;
			var input = PathOf(str_ios[j + 0], result.root_directory, tmp_buffer);
			result.io[i].input = StringAlloc(input);
			result.io[i].raw_input = StringAlloc(str_ios[j + 0]);
			var output = PathOf(str_ios[j + 1], result.root_directory, tmp_buffer);
			result.io[i].output = StringAlloc(output);
			result.io[i].raw_output = StringAlloc(str_ios[j + 1]);
		}
	}
	else
	{
		crash("No files selected\n\n" ARGUMENTS_HELP);
	}

	if (watches_count)
	{
		result.watch_count = watches_count;
		result.watch_directories = alloc(string, watches_count);
		for (var i = 0; i < watches_count; i++)
		{
			var directory = String(str_watches[i], tmp_buffer);
			Terminate(directory);
			CleanupPath(directory);
			result.watch_directories[i] = StringAlloc(directory);
		}
	}
	else
	{
		result.watch_count = 0;
		result.watch_directories = null;
	}

	//pool->current = watches_buffer;





	print("exe path:          " CC_SUCCESS CC_UNDERLINE "%s", result.exe_path.data);
	print("exe directory:     " CC_SUCCESS CC_UNDERLINE "%s", result.exe_directory.data);
	print("root directory:    " CC_SUCCESS CC_UNDERLINE "%s", result.root_directory.data);
	if (result.watch_count == 1)
	{
		print("watch directory:   " CC_SUCCESS CC_UNDERLINE "%s", result.watch_directories[0].data);
	}
	else
	{
		print("watch directories:");
		for (var i = 0; i < result.watch_count; i++)
			print("\t%s", result.watch_directories[i].data);
	}
	for (var i = 0; i < result.io_count; i++)
		print("Converting " CC_SUCCESS CC_UNDERLINE "%s" CC_RESTORE " > " CC_SUCCESS CC_UNDERLINE "%s", result.io[i].input.data, result.io[i].output.data);





	return result;
}
