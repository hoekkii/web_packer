inline s32
DirectoryUp(string s, s32 amount)
{
	s32 i = s.length - 1;
	while (amount > 0)
	{
		s32 x = 0;
		while (i >= 0)
		{
			if (s[i] == '/' || s[i] == '\\')
			{
				if (x) x = 2;
			}
			else if (x == 2) break;
			else x = 1;

			i--;
		}

		amount--;
	}

	return i >= 0 ? i + 1 : s.length;
}

inline s32
DirectoryUp(string s)
{
	s32 x = 0;
	s32 i = s.length - 1;
	while (i >= 0)
	{
		if (s[i] == '/' || s[i] == '\\')
		{
			if (x) x = 2;
		}
		else if (x == 2) return i;
		else x = 1;

		i--;
	}

	return s.length;
}

inline void
CleanupPath(string& path)
{
	s32 c = path.capacity - path.length;
	s32 l = 0;
	assert(c > path.length);
	var p = _fullpath(path.data_char + path.length, path.data_cchar, c);
	for (var i = path.length; i < path.capacity; i++)
	{
		if (path[i] == '\\')
			path[i] = '/';
		else if (path[i] == '\0')
		{
			l = i - path.length;
			break;
		}
	}

	memmove(path.data, path.data + path.length, l);
	path.length = l;
	path.data[l] = '\0';
}

static string
PathOf(string filename, string root, static_buffer* data)
{
	var result = string{ data->uchardata, 0, WEB_PACKER_TEMP_SIZE };
	Append(result, root);
	Append(result, filename);
	Terminate(result);
	CleanupPath(result);
	return result;
}


const string DirectoryOf(string file)
{
	for (int i = file.length - 1; i >= 0; --i)
	{
		if (file[i] == '/')
			return Substring(file, 0, i);
	}

	CRASH;
}

const string ExtensionOf(string file)
{
	for (int i = file.length - 1; i >= 0; --i)
	{
		assert(file[i] != '/');
		if (file[i] == '.')
			return Substring(file, i + 1);
	}

	CRASH;
}




