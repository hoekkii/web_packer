#ifndef INNER_TYPE
#define INNER_TYPE TYPE
#endif

#ifndef TYPE_POOL_CAPACITY
#define TYPE_POOL_CAPACITY SOLITUDE_TEMP_COLLECTION_SIZE
#endif

#ifndef TYPE_POOL
#define TYPE_POOL PPCAT(TYPE, _pool)
#endif

#ifndef POOL_TYPE_NAME
#define POOL_TYPE_NAME PPCAT(TYPE_NAME, Pool)
#endif

#ifndef TYPE_NEXT
#define TYPE_NEXT next
#endif

struct TYPE_POOL
{
	static s32 const capacity = TYPE_POOL_CAPACITY;

	INNER_TYPE* current;
	INNER_TYPE* first;
	TYPE_POOL* next;
};

static INNER_TYPE*
TYPE_NAME(TYPE_POOL* pool)
{
	if (!pool->current->TYPE_NEXT)
	{
		INNER_TYPE* items = alloc(INNER_TYPE, TYPE_POOL_CAPACITY);
		for (var i = 1; i < TYPE_POOL_CAPACITY; i++)
			items[i - 1].TYPE_NEXT = &items[i];
		items[TYPE_POOL_CAPACITY - 1].TYPE_NEXT = null;
		pool->current->TYPE_NEXT = items;

		TYPE_POOL* next_pool = alloc(TYPE_POOL);
		next_pool->current = null;
		next_pool->first = items;
		next_pool->next = null;

		TYPE_POOL* last_pool = pool;
		while (last_pool->next)
			last_pool = last_pool->next;
		last_pool->next = next_pool;
	}

	var result = pool->current;
	pool->current = result->TYPE_NEXT;
	return result;
}

static void
Reset(TYPE_POOL* pool)
{
	for (;;)
	{
		INNER_TYPE* items = pool->first;
		for (var i = 1; i < TYPE_POOL_CAPACITY; i++)
			items[i - 1].TYPE_NEXT = &items[i];

		pool->current = items;
		if (pool->next)
		{
			pool = pool->next;
			items[TYPE_POOL_CAPACITY - 1].TYPE_NEXT = pool->first;
		}
		else
		{
			items[TYPE_POOL_CAPACITY - 1].TYPE_NEXT = null;
			break;
		}
	}
}

static TYPE_POOL*
POOL_TYPE_NAME()
{
	INNER_TYPE* items = alloc(INNER_TYPE, TYPE_POOL_CAPACITY);
	for (var i = 1; i < TYPE_POOL_CAPACITY; i++)
		items[i - 1].TYPE_NEXT = &items[i];
	items[TYPE_POOL_CAPACITY - 1].TYPE_NEXT = null;

	TYPE_POOL* pool = alloc(TYPE_POOL);
	pool->current = items;
	pool->first = items;
	pool->next = null;
	return pool;
}

#undef INNER_TYPE
#undef TYPE
#undef TYPE_NAME
#undef TYPE_POOL
#undef TYPE_NEXT
#undef POOL_TYPE_NAME