// TODO: Join, Compare, EqualsIgnoreCase, ToCstring?, IndexOf

inline string
String(void* memory, s32 capacity)
{
	string result;
	result.length = 0;
	result.capacity = capacity;
	result.data_void = memory;
	return result;
}
/*inline string
String(s32 capacity)
{
	return String(malloc(capacity), capacity);
}*/
inline string
String(static_buffer* buffer)
{
	string result;
	result.length = 0;
	result.capacity = WEB_PACKER_TEMP_SIZE;
	result.data_void = buffer->data;
	return result;
}

inline string
String(string s, void* memory, s32 capacity)
{
	string result;
	result.length = s.length;
	result.capacity = capacity;
	result.data_void = memory;
	memcpy(result.data, s.data, result.length);
	return result;
}
inline string
String(string s, static_buffer* buffer)
{
	return String(s, buffer->data, WEB_PACKER_TEMP_SIZE);
}


inline string
String(const void* s, s32 length, void* memory, s32 capacity)
{
	assert(s);

	string result;
	result.length = length;
	result.capacity = capacity;
	result.data_void = memory;
	memcpy(memory, s, length);
	return result;
}
/*inline string
String(const void* s, s32 length)
{
	return String(s, length, malloc(length), length);
}*/
/*inline string
String(const char* s)
{
	var length = strlen(s);
	return String(s, length, malloc(length), length);
}*/
inline string
String(const char* s, s32 length, static_buffer* buffer)
{
	return String(s, length, buffer->data, WEB_PACKER_TEMP_SIZE);
}
inline string
String(const char* s, static_buffer* buffer)
{
	return String(s, strlen(s), buffer->data, WEB_PACKER_TEMP_SIZE);
}

inline const string
StringNonAlloc(const char* s, s32 length)
{
	string result;
	result.length = length;
	result.capacity = result.length + 1;
	result.data_cchar = s;
	return result;
}
inline const string
StringNonAlloc(const char* s)
{
	return StringNonAlloc(s, strlen(s));
}

inline string
StringAlloc(string s)
{
	var length = s.length;
	string result;
	result.length = length;
	result.capacity = length + 1;
	result.data_void = malloc(result.capacity);
	memcpy(result.data_void, s.data_void, length);
	result.data[length] = '\0';
	return result;
}

inline string
StringAlloc(const char* s)
{
	var length = strlen(s);
	string result;
	result.length = length;
	result.capacity = length + 1;
	result.data_void = malloc(result.capacity);
	memcpy(result.data_void, s, length);
	result.data[length] = '\0';
	return result;
}


inline const char*
ToC(string s)
{
	var cstr = alloc(char, s.length + 1);
	memcpy(cstr, s.data, s.length);
	cstr[s.length] = '\0';
	return cstr;
}
inline void
ToC(string& s, void* destination)
{
	memcpy(destination, s.data, s.length);
	reinterpret_cast<char*>(destination)[s.length] = '\0';
}
inline void
TerminateAt(string s, s32 length)
{
	assert(length < s.length);
	s.length = length;
	s.data[length] = '\0';
}
inline void
Terminate(string s)
{
	assert(s.length < s.capacity);
	s.data[s.length] = '\0';
}



inline void
Free(string& s)
{
	if (s.capacity < 1)
		return;

	free(s.data);
	s.length = 0;
	s.capacity = 0;
}
inline void
EnsureCapacity(string& s, s32 capacity)
{
	if (s.capacity < capacity)
	{
		s.data = reinterpret_cast<uchar*>(realloc(s.data, capacity));
		s.capacity = capacity;
	}
}


inline void
Append(string& l, char r)
{
	assert(l.length < l.capacity);
	l.data[l.length++] = r;
}
inline void
Append(string& l, const char* r, s32 length)
{
	s32 total = length + l.length;
	assert(l.capacity >= total);
	memcpy(l.data + l.length, r, length);
	l.length += length;
}
inline void
Append(string& l, const char* r) { Append(l, r, strlen(r)); }
inline void
Append(string& l, string r) { Append(l, r.data_cchar, r.length); }



inline void
DynamicAppend(string& l, char r)
{
	s32 total = l.length + 1;
	if (total < l.capacity)
	{
		l.data[l.length++] = r;
	}
	else
	{
		l.data = reinterpret_cast<uchar*>(realloc(l.data, total));
		l.data[l.length++] = r;
		l.capacity = total;
	}
}
void
DynamicAppend(string& l, const char* r, s32 length)
{
	s32 total = length + l.length;
	if (l.capacity >= total)
	{
		memcpy(l.data + l.length, r, length);
		l.length += length;
	}
	else
	{
		l.data = reinterpret_cast<uchar*>(realloc(l.data, total));
		memcpy(l.data + l.length, r, length);
		l.length = total - 1;
		l.capacity = total;
	}

}
inline void
DynamicAppend(string& l, const char* r) { DynamicAppend(l, r, strlen(r)); }
inline void
DynamicAppend(string& l, string r) { DynamicAppend(l, r.data_cchar, r.length); }






// TODO: Should we account for weird accents stuff?
inline bool
IsLower(uchar c) { return c >= 'a' && c <= 'z'; }
inline uchar
ToLower(uchar c) { return c >= 'A' && c <= 'Z' ? c + ('a' - 'A') : c; }
inline bool
IsUpper(uchar c) { return c >= 'A' && c <= 'Z'; }
inline uchar
ToUpper(uchar c) { return c >= 'a' && c >= 'z' ? c - ('a' - 'A') : c; }

inline bool
IsLower(char c) { return c >= 'a' && c <= 'z'; }
inline char
ToLower(char c) { return c >= 'A' && c <= 'Z' ? c + ('a' - 'A') : c; }
inline bool
IsUpper(char c) { return c >= 'A' && c <= 'Z'; }
inline char
ToUpper(char c) { return c >= 'a' && c >= 'z' ? c - ('a' - 'A') : c; }

inline void
ToUpper(string s)
{
	for (s32 i = 0; i < s.length; i++)
	{
		uchar& c = s.data[i];
		if (IsLower(c))
			c = c - ('a' - 'A');
	}
}

inline void
ToLower(string s)
{
	for (s32 i = 0; i < s.length; i++)
	{
		uchar& c = s.data[i];
		if (IsUpper(c))
			c = c + ('a' - 'A');
	}
}














inline s32
Hash(const char* s)
{
	u32 result = 0;
	while (*s)
		result = 101 * result + *s++;

	return result;
}


inline s32
Hash(const string& s)
{
	s32 result = 0;
	for (s32 i = 0; i < s.length; i++)
		result = 101 * result + s[i];

	return result;
}


inline bool
Equals(string l, string r)
{
	return l.length == r.length
		&& Equals(l.data, r.data, r.length);
}

inline bool
Equals(const char* l, const char* r)
{
	var length = strlen(l);
	return length  == strlen(r)
		&& Equals(l, r, length);
}


inline bool
Equals(string l, string r, s32 length)
{
	return Equals(l.data, r.data, length);
}

inline bool
Equals(string l, const char* r)
{
	for (s32 i = 0; i < l.length; i++)
	{
		if (*reinterpret_cast<const uchar*>(r++) != l[i])
			return false;
	}

	return *r == '\0';
}

inline bool
Equals(string l, const char* r, s32 length)
{
	for (s32 i = 0; i < length; i++)
	{
		if (*reinterpret_cast<const uchar*>(r++) != l[i])
			return false;
	}

	return true;
}



inline const string
Substring(string s, s32 start, s32 length)
{
	assert(start >= 0);
	assert(start <= s.length);
	assert(length >= 0);
	assert(start <= s.length - length);

	string result;
	result.length = length;
	result.capacity = length;
	result.data = &s.data[start];
	return result;
}
inline const string
Substring(string s, s32 start) { return Substring(s, start, s.length - start); }

inline bool
IsEmpty(string s) { return s.length <= 0; }

inline bool
Filled(string s) { return s.length > 0; }

inline string&
operator += (string& l, const char r) { Append(l, r); return l; }

inline string&
operator += (string& l, const char* r) { Append(l, r); return l; }

inline string&
operator += (string& l, string& r) { Append(l, r); return l; }

inline bool
operator == (string& l, string& r) { return Equals(l, r); }

inline bool
operator == (string& l, const char* r) { return Equals(l, r); }

inline bool
operator == (const char* l, string& r) { return Equals(r, l); }





inline bool
IsWhitespace(char c)
{
	return c == ' '
		|| c == '\n'
		|| c == '\r'
		|| c == '\t';
}

