
static file*
pre_tokenize(string filename, context* ctx, token_pool* pool)
{
	for (var i = 0; i < ctx->file_count; i++)
	{
		if (Equals(ctx->files[i].name, filename))
			return &ctx->files[i];
	}

	if (ctx->file_count >= ctx->file_capacity)
	{
		ctx->t.error = "Exceeded max file reads";
		print_error("Exceeded max file reads");
		return null;
	}

	var name = StringAlloc(filename);
	var fh = fopen(name.data_cchar, "rb");
	if (!fh)
	{
		ctx->t.error = "File not found";
		print_error("Could not find file: %s", name.data_cchar);
		return null;
	}

	fseek(fh, 0, SEEK_END);
	s32 size = ftell(fh);
	var data = alloc(uchar, size);
	var content = string{ data, size, size };

	fseek(fh, 0, SEEK_SET);
	fread(data, 1, size, fh);
	fclose(fh);

	file* result = &ctx->files[ctx->file_count];
	result->name = name;
	result->content = content;
	ctx->file_count++;
	print("Reading %s..", __PRINT_CLEAN_FILE(name.data_cchar))

	// Parse file
	{
		token* preprocess = null;
		token* first = null;
		token* previous = null;
		token* eval = null;

		s32 line = 1;
		s32 column = 0;

		var create_token = [pool, result, &line, &column, &first, &previous, &eval]()
		{
			eval = Token(pool);
			if (!first) first = eval;
			if (previous) previous->next = eval;
			previous = eval;
			eval->origin = result;
			eval->line = line;
			eval->column = column;
			eval->next = null;
		};

		for (var i = 0; i < size;)
		{
			var ptr = data + i;
			var c = *ptr;

			if (eval)
			{
				switch (eval->type)
				{
					case token::PRE_UNPARSED:
					{
						if (IsWhitespace(c) || (i < size - 2 && Equals(ptr, "@{", 2)))
						{
							eval = null;
						}
						else
						{
							eval->content.length += 1;
							eval->content.capacity = eval->content.length;
							column++;
							i++;
						}
					} break;
					case token::SPACE:
					{
						if (c == '\n' || c == '\r')
						{
							var count = i < size - 2 && (Equals(ptr, "\n\r", 2) || Equals(ptr, "\r\n", 2)) ? 2 : 1;
							eval->content.length += count;
							eval->content.capacity = eval->content.length;
							column = 0;
							line++;
							i += count;
						}
						else if (IsWhitespace(c))
						{
							eval->content.length += 1;
							eval->content.capacity = eval->content.length;
							column++;
							line++;
							i += 1;
						}
						else
						{
							eval = null;
						}
					} break;
					case token::IDENTIFIER:
					{
						if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_')
						{
							eval->content.length += 1;
							eval->content.capacity = eval->content.length;
							column++;
							i++;
						}
						else
						{
							eval = null;
						}
					} break;
					case token::STRING:
					{
						// Check if we need to escape a character
						if (i < size - 2 && c == '\\')
						{
							eval->content.length += 2;
							eval->content.capacity = eval->content.length;
							column += 2;
							i += 2;
						}
						else
						{
							eval->content.length += 1;
							eval->content.capacity = eval->content.length;
							column++;
							i++;

							if (c == eval->string_type)
							{
								//print("%.*s", eval->content.length, eval->content.data)
								eval = null;
							}
						}
					} break;
				}
			}
			else if (IsWhitespace(c))
			{
				create_token();
				eval->content = string{ ptr, 0, 0 };
				eval->type = token::SPACE;
			}
			else if (preprocess && (c == '\'' || c == '\"'))
			{
				create_token();
				eval->content = string{ ptr, 1, 1 };
				eval->type = token::STRING;
				eval->string_type = c;
				column++;
				i++;
			}
			else if (i < size - 2 && Equals(ptr, "@{", 2))
			{
				if (preprocess)
				{
					ctx->t.error = "Unexpected token";
					print_error("Unexpected token: @{. Already in preprocess mode");
					return null;
				}

				create_token();
				eval->content = string{ ptr, 2, 2 };
				eval->type = token::PRE_OPEN;
				preprocess = eval;
				eval = null;
				column += 2;
				i += 2;
			}
			else if (preprocess && c == '}')
			{
				create_token();
				eval->content = string{ ptr, 1, 1 };
				eval->type = token::PRE_CLOSED;
				eval = null;
				preprocess = null;
				column++;
				i++;
			}
			else if (preprocess && ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_'))
			{
				create_token();
				eval->content = string{ ptr, 0, 0 };
				eval->type = token::IDENTIFIER;
			}
			else
			{
				create_token();
				eval->content = string{ ptr, 0, 0 };
				eval->type = token::PRE_UNPARSED;
			}
		}

		if (preprocess)
		{
			ctx->t.error = "Preprocessing did not close";
			print_error(
				"[%s %i:%i] File did not close preprocessing",
				__PRINT_CLEAN_FILE(name.data_cchar),
				preprocess->line,
				preprocess->column);
			return null;
		}

		if (eval)
		{
			if (eval->type == token::STRING)
			{
				ctx->t.error = "String did not close";
				print_error(
					"[%s %i:%i] String did not close: %s",
					__PRINT_CLEAN_FILE(name.data_cchar),
					eval->line,
					eval->column,
					name.data);
				return null;
			}
		}
		else
		{
			print_warning(
				"[%s] File did not have any tokens",
				__PRINT_CLEAN_FILE(name.data_cchar));

			create_token();
			eval->content = string{ null, 0, 0 };
			eval->type = token::SPACE;
		}

		result->first = first;
	}

	return result;
}

inline void
CreateToken(context* ctx, token_pool* pool, token* from, s32 offset)
{
	var eval = Token(pool);
	if (!ctx->t.first) ctx->t.first = eval;
	if (ctx->t.previous) ctx->t.previous->next = eval;
	if (from)
	{
		eval->type = from->type;
		eval->origin = from->origin;
		eval->line = from->line;
		eval->column = from->column + offset;
		eval->content = from->content;
		eval->content.data += offset;
		eval->content.length -= offset;
		eval->content.capacity = eval->content.length;
	}
	eval->next = null;
	ctx->t.eval = eval;
	ctx->t.previous = eval;
}

inline bool
IsOperator(char c)
{
	switch (c)
	{
		case '+':
		case '-':
		case '*':
		case '/':
		case '%':
		case '^':
		case '=':
		case '!':
		case '>':
		case '<':
		case '?':
		case ':':
		case '&':
		case '|':
		case '~':
		case '(':
		case ')':
		case '[':
		case ']':
		case '{':
		case '}':
		case ',':
		case '.':
			return true;
	}
	return false;
}

static int
tokenize(file* input_file, context* ctx, token_pool* pool, static_buffer* tmp, s32 depth)
{
	if (depth > 50)
	{
		ctx->t.error = "Safety depth error triggered";
		print_error("Possible infinite include happening: %.*s", input_file->name.length, input_file->name.data_cchar);
		return 1;
	}

	token* input = input_file->first;
	while (input)
	{
		switch (input->type)
		{
			default:
			{
				ctx->t.error = "Unexpected token";
				print_error(
					"[%s %i:%i] Unexpected token: %.*s",
					__PRINT_CLEAN_FILE(input->origin->name.data_cchar),
					input->line,
					input->column,
					input->content.length,
					input->content.data);
			} return 1;

			case token::PRE_UNPARSED:
			{
				var content = input->content;
				for (var i = 0; i < content.length;)
				{
					var c = content[i];
					if (ctx->t.eval)
					{

					}
					else if (IsOperator(c))
					{
						CreateToken(ctx, pool, input, i);
						ctx->t.eval->type = token::OPERATOR;
						ctx->t.eval->operator_type = c;
						ctx->t.eval = null;
					}
					else if (c == '\'' || c == '`' || c == '"')
					{
						// String start
					}
					else if (c >= '0' && c <= '9')
					{
						// Number start
					}
					else
					{
						// Identifier
					}
				}

				if (ctx->t.eval)
				{

				}
				else
				{
					CreateToken(ctx, pool, input, 0);
					ctx->t.eval = null;
				}

				input = input->next;
			} break;

			case token::SPACE:
			{
				CreateToken(ctx, pool, input, 0);
				ctx->t.eval = null;
				input = input->next;
			} break;

			case token::PRE_OPEN:
			{
				token* identifier = null;
				input = input->next;
				while (input && input->type != token::PRE_CLOSED)
				{
					if (input->type != token::SPACE)
					{
						if (identifier)
						{
							if (Equals(identifier->content, "include"))
							{
								var inner = string{ input->content.data + 1, input->content.length - 2, input->content.length - 2 };
								var path = PathOf(inner, ctx->root_directory, tmp);
								var f = pre_tokenize(path, ctx, pool);
								if (ctx->t.error)
								{
									return 1;
								}
								if (f)
								{
									tokenize(f, ctx, pool, tmp, depth + 1);
									if (ctx->t.error)
									{
										return 1;
									}
								}
								else
								{
									return 1;
								}
							}
							else
							{
								ctx->t.error = "Unexpected identifier";
								print_error(
									"[%s %i:%i] Unexpected identifier: %.*s",
									__PRINT_CLEAN_FILE(input->origin->name.data_cchar),
									input->line,
									input->column,
									identifier->content.length,
									identifier->content.data);
								return 1;
							}

							identifier = null;
						}
						else if (input->type == token::IDENTIFIER)
						{
							identifier = input;
						}
						else
						{
							print_error("Unexpected");
						}
					}

					input = input->next;
				}

				if (input && input->type == token::PRE_CLOSED) input = input->next;
				else print_warning("Not closed correctly!");
			} break;
		}
	}

	return 0;
}
