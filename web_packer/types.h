// Base types
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int32_t b32;
typedef float r32;
typedef double r64;
typedef intptr_t intptr;
typedef uintptr_t uintptr;
typedef unsigned char uchar;

// Defines
#define null nullptr
#define var auto
#define let auto

// Constants
#define PI 3.141592653589793f
#define TAU 6.283185307179586f
#define DEG2RAD 0.017453292519943295f
#define RAD2DEG 57.29577951308232f
#define EPSILON 0.00001f
#define S8MIN  -128
#define S16MIN -32768
#define S32MIN -2147483648
#define S64MIN -9223372036854775808L
#define S8MAX  127
#define S16MAX 32767
#define S32MAX 2147483647
#define S64MAX 9223372036854775807L
#define U8MAX  255
#define U16MAX 65535
#define U32MAX 4294967295U
#define U64MAX 18446744073709551615UL

// assert and s(tatic)_assert
#define CRASH *(int*)0=0
#if WEB_PACKER_DEBUG
#define assert(expr) if (!(expr)) { fprintf(stderr, "FAILED assertion [" __FILE__ ":%i] " #expr "\n", __LINE__);  CRASH; }
#define s_assert(expr) static_assert(expr, "FAILED assertion: " #expr)
#else
#define assert(expr)
#define s_assert(expr)
#endif

#if WEB_PACKER_INTERNAL
#define NotImplemented assert(!"NotImplemented")
#else
#define NotImplemented NotImplemented!
#endif



#define CC_RESTORE		"\x1B[m"
#define CC_INFO			"\x1B[0m"
#define CC_ERROR		"\x1B[31m"
#define CC_WARNING		"\x1B[33m"
#define CC_SUCCESS		"\x1B[32m"
#define CC_UNDERLINE	"\x1B[4m"
#define CC_MARK			"\x1B[7m"
const char* __PRINT_CLEAN_FILE(const char* c)
{
	const char* result = c;
	for (const char* i = c; *i; ++i)
	{
		if (*i == '/' || *i == '\\')
			result = i + 1;
	}
	return result;
}
#define print(...) { fprintf(stderr, "\x1B[4m\x1B[36m%s:%i\x1B[m ", __PRINT_CLEAN_FILE(__FILE__), __LINE__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, CC_RESTORE "\n"); }
#define print_error(...) { fprintf(stderr, "\x1B[4m\x1B[36m%s:%i\x1B[m " CC_ERROR "ERROR:\x1B[m ", __PRINT_CLEAN_FILE(__FILE__), __LINE__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, CC_RESTORE "\n"); }
#define print_warning(...) { fprintf(stderr, "\x1B[4m\x1B[36m%s:%i\x1B[m " CC_WARNING "WARNING:\x1B[m ", __PRINT_CLEAN_FILE(__FILE__), __LINE__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, CC_RESTORE "\n"); }
#define print_info(...) { fprintf(stderr, "\x1B[4m\x1B[36m%s:%i\x1B[m " CC_INFO "INFO:\x1B[m " , __PRINT_CLEAN_FILE(__FILE__), __LINE__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, CC_RESTORE "\n"); }
#define print_success(...) { fprintf(stderr, "\x1B[4m\x1B[36m%s:%i\x1B[m " CC_SUCCESS "SUCCESS:\x1B[m " , __PRINT_CLEAN_FILE(__FILE__), __LINE__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, CC_RESTORE "\n"); }
#define crash(...) { \
	char ___buffer[1024]; \
	char* ___buffer_ptr = ___buffer; \
	___buffer_ptr += sprintf(___buffer_ptr, "%s:%i\n", __PRINT_CLEAN_FILE(__FILE__), __LINE__); \
	___buffer_ptr += sprintf(___buffer_ptr, __VA_ARGS__); \
	fprintf(stderr, "\x1B[4m\x1B[36m%s:%i\x1B[m " CC_ERROR "ERROR:\x1B[m ", __PRINT_CLEAN_FILE(__FILE__), __LINE__); \
	fprintf(stderr, __VA_ARGS__); \
	fprintf(stderr, CC_RESTORE "\n"); \
	/*SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Solitude", ___buffer, null);*/ \
	exit(1); \
}


#define PPCAT_NX(A, B) A ## B
#define PPCAT(A, B) PPCAT_NX(A, B)

#define ARGUMENT_AT_0(arg0, ...) arg0
#define ARGUMENT_AT_1(arg0, arg1, ...) arg1
#define ARGUMENT_AT_2(arg0, arg1, arg2, ...) arg2
#define ARGUMENT_AT_3(arg0, arg1, arg2, arg3, ...) arg3
#define ARGUMENT_AT_4(arg0, arg1, arg2, arg3, arg4, ...) arg4
#define ARGUMENT_AT_0_(tuple) ARGUMENT_AT_0 tuple
#define ARGUMENT_AT_1_(tuple) ARGUMENT_AT_1 tuple
#define ARGUMENT_AT_2_(tuple) ARGUMENT_AT_2 tuple
#define ARGUMENT_AT_3_(tuple) ARGUMENT_AT_3 tuple
#define ARGUMENT_AT_4_(tuple) ARGUMENT_AT_4 tuple

#if defined(_WIN32) || defined(WIN32)
#define For_0(arr, i) for (s32 i = 0; i < arr.length; ++i)
#define For_1(arr) for (s32 i = 0; i < arr.length; ++i)
#define For(arr, ...) ARGUMENT_AT_2_((__VA_ARGS__, For_1(arr, __VA_ARGS__), For_0(arr, __VA_ARGS__)))
#define Forr_0(arr, i) for (s32 i = arr.length - 1; i >= 0; --i)
#define Forr_1(arr) for (s32 i = arr.length - 1; i >= 0; --i)
#define Forr(arr, ...) ARGUMENT_AT_2_((__VA_ARGS__, Forr_1(arr, __VA_ARGS__), Forr_0(arr, __VA_ARGS__)))
#define alloc_0(type) reinterpret_cast<##type*>(malloc(sizeof(##type)))
#define alloc_1(type, count) reinterpret_cast<##type*>(malloc(sizeof(##type)*(count)))
#define alloc(...) ARGUMENT_AT_2_((__VA_ARGS__, alloc_1(__VA_ARGS__), alloc_0(__VA_ARGS__)))
#elif defined(__unix__)
#define For_0(arr, i) for (s32 i = 0; i < arr.length; ++i)
#define For_1(arr) for (s32 i = 0; i < arr.length; ++i)
#define For(arr, ...) ARGUMENT_AT_2(,##__VA_ARGS__, For_0(arr, __VA_ARGS__), For_1(arr, __VA_ARGS__))
#define Forr_0(arr, i) for (s32 i = arr.length - 1; i >= 0; --i)
#define Forr_1(arr) for (s32 i = arr.length - 1; i >= 0; --i)
#define Forr(arr, ...) ARGUMENT_AT_2(,##__VA_ARGS__, Forr_0(arr, __VA_ARGS__), Forr_1(arr, __VA_ARGS__))
#define alloc_0(type) reinterpret_cast<type*>(malloc(sizeof(type)))
#define alloc_1(type, count) reinterpret_cast<type*>(malloc(sizeof(type)*(count)))
#define alloc_i(...) ARGUMENT_AT_2(__VA_ARGS__, alloc_1, alloc_0, )
#define alloc(...) alloc_i(__VA_ARGS__)(__VA_ARGS__)
#else
#endif


struct string
{
	union
	{
		uchar* data;
		u8* data_u8;
		char* data_char;
		const char* data_cchar;
		void* data_void;
	};
    
	s32 length;
	s32 capacity;

	inline uchar& operator [] (s32 i)
	{
		assert(i >= 0);
		assert(i < this->capacity);
		return this->data[i];
	}

	inline uchar operator [] (s32 i) const
	{
		assert(i >= 0);
		assert(i < this->capacity);
		return this->data[i];
	}
};

struct static_buffer
{
	static_buffer* next;
	union
	{
		u8 data[WEB_PACKER_TEMP_SIZE];
		u8 u8data[WEB_PACKER_TEMP_SIZE];
		char chardata[WEB_PACKER_TEMP_SIZE];
		uchar uchardata[WEB_PACKER_TEMP_SIZE];
	};
};
#define TYPE static_buffer
#define TYPE_NAME StaticBuffer
#include "memory_pool.hpp"





struct input_output
{
	string input;
	string output;
	string raw_input;
	string raw_output;
};

struct program_arguments
{
	string exe_path;
	string exe_directory;
	string root_directory;
	string* watch_directories;
	s32 watch_count;
	input_output* io; // TODO: Think of a better name
	s32 io_count;
	
	enum
	{
		NONE = 0,
		DEBUG_OUTPUT = 1 << 0,
		BEAUTIFY_OUTPUT = 1 << 1,
		MINIFY_OUTPUT = 1 << 2,
	} flags;
};

struct token;
struct file
{
	string name;
	string content;
	token* first;
};

struct token
{
	token* _next;
	token* next;
	file* origin;
	s32 line;
	s32 column;
	string content;

	enum : u32
	{
		PRE_UNPARSED,
		PRE_OPEN,
		PRE_CLOSED,

		SPACE,
		OPERATOR,
		IDENTIFIER,
		STRING,
		NUMBER,
	} type;

	union
	{
		uchar string_type;
		uchar operator_type;
	};
};

#define TYPE token
#define TYPE_NAME Token
#define TYPE_NEXT _next
#include "memory_pool.hpp"


struct attribute
{
	attribute* next;
	string tag;
	string value;
};

#define TYPE attribute
#define TYPE_NAME Attribute
#include "memory_pool.hpp"

struct node
{
	node* next;
	node* parent;
	attribute* attributes;
	void* token;
    string tag;
    string content;

	enum : b32
	{
		NONE						= 0,

		ROOT						= 1 << 0,
		DOCTYPE						= 1 << 1, // <!DOCTYPE
		CSS							= 1 << 2, // <style>...</style> <script type="text/css">
		JS							= 1 << 3,
		JSON						= 1 << 4,

		BUILDING_TAG				= 1 << 16,
		BUILDING_NODE				= 1 << 17,
		BUILDING_ATTRIBUTE_TAG		= 1 << 18,
		BUILDING_ATTRIBUTE_VALUE	= 1 << 19,
		BUILDING_CLOSING			= 1 << 20,

		// Special
		BUILDING 					= -1 << 16,
	} flags;
};

#define TYPE node
#define TYPE_NAME Node
#include "memory_pool.hpp"

struct context
{
	string root_directory;

	file* files;
	s32 file_count;
	s32 file_capacity;

	union
	{
		struct
		{
			token* first;
			token* previous;
			token* eval;
			const char* error;
		} t;
		struct
		{
			token* first;
			node* current;
		} n;
	};
};
