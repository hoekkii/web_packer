#define _CRT_SECURE_NO_DEPRECATE
#define WEB_PACKER_INTERNAL 1
#define WEB_PACKER_DEBUG 1
#define WEB_PACKER_TEMP_SIZE (1 * 1024)
#define SOLITUDE_TEMP_COLLECTION_SIZE 1024

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <tchar.h>
#if 0
#include <ctime>
#define clock_to_ms(x) 
inline double diff_time_ms(clock_t t) { return (clock() - t) / (CLOCKS_PER_SEC / 1000.0); }
#else
#include <chrono>
#define clock_t std::chrono::steady_clock::time_point
#define clock() std::chrono::high_resolution_clock::now()
inline double
diff_time_ms(clock_t t) { return std::chrono::duration_cast<std::chrono::nanoseconds>(clock() - t).count() / 1000000.0; }
#endif

#include "types.h"
#include "memory.hpp"
#include "string.hpp"
#include "file.hpp"
#include "arguments.hpp"
#include "tokenizer.hpp"

static void write(token* token, string file)
{
	var f = fopen(file.data_cchar, "wb");
	while (token)
	{
		fwrite(token->content.data, 1, token->content.length, f);
		token = token->next;
	}
	fclose(f);
}

static void
parse(
	program_arguments* arguments,
	static_buffer_pool* bufferpool,
	token_pool* tokenpool,
	node_pool* nodepool,
	attribute_pool* attributepool,
	file* filepool,
	s32 file_capacity)
{
	var begin0 = clock();

	print("PARSE\n");

	Reset(bufferpool);
	Reset(tokenpool);
	Reset(nodepool);
	Reset(attributepool);

	node* root = Node(nodepool);
	root->flags = node::ROOT;

	var tmp = StaticBuffer(bufferpool);
	context ctx;
	ctx.root_directory = arguments->root_directory;
	ctx.file_count = 0;
	ctx.file_capacity = file_capacity;
	ctx.files = filepool;

	print("init: %.5lf ms", diff_time_ms(begin0));
	var begin1 = clock();
	for (var i = 0; i < arguments->io_count; i++)
	{
		var io = arguments->io[i];
		print("converting %s to %s", io.raw_input.data, io.raw_output.data);

		var begin2 = clock();
		Reset(tokenpool);
		ctx.t.first = null;
		ctx.t.previous = null;
		ctx.t.eval = null;
		ctx.t.error = null;

		var input_file = pre_tokenize(io.input, &ctx, tokenpool);
		print("pre_tokenize: %.5lf ms", diff_time_ms(begin2));
		if (ctx.t.error)
			continue;

		var begin3 = clock();
		tokenize(input_file, &ctx, tokenpool, tmp, 0);
		print("tokenize: %.5lf ms", diff_time_ms(begin3));
		if (ctx.t.error)
			continue;

		//write(f->first, io.output);
		write(ctx.t.first, io.output);
		print("iteration: %.5lf ms", diff_time_ms(begin2));
	}
	print("loop: %.5lf ms", diff_time_ms(begin1) * 1000.0);

	for (var i = 0; i < ctx.file_count; i++)
		Free(ctx.files[i].content);

	print("total: %.5lf ms", diff_time_ms(begin0) * 1000.0);
}

int main(s32 argc, const char* args[])
{
	#if WEB_PACKER_DEBUG
	{
		var tmp_args0 = args[0];
		argc = 10;
		args = new const char* [10];
		args[0] = tmp_args0;
		args[1] = "-i";
		args[2] = "index.html";
		args[3] = "../output/index.html";
		args[4] = "-w";
		args[5] = "C:/Users/sietse/Projects/web_packer/test/input/";
		args[6] = "-r";
		args[7] = "C:/Users/sietse/Projects/web_packer/test/input/";
		args[8] = "-o";
		args[9] = "debug";
	}
	#endif

	assert(sizeof(char) == 1);
	assert(argc > 0);
	var bufferpool = StaticBufferPool();
	var arguments = RetrieveProgramArguments(argc, args, bufferpool);
	var tokenpool = TokenPool();
	var nodepool = NodePool();
	var attributepool = AttributePool();
	var file_capacity = 64;
	var filepool = alloc(file, file_capacity);

	parse(&arguments, bufferpool, tokenpool, nodepool, attributepool, filepool, file_capacity);
	if (!arguments.watch_count)
	{
		print("No watch directories selected. Exiting..")
		return 0;
	}

	var handles = alloc(HANDLE, arguments.watch_count);
	for (var i = 0; i < arguments.watch_count; i++)
	{
		handles[i] = FindFirstChangeNotification(
			arguments.watch_directories[i].data_cchar,
			TRUE,
			FILE_NOTIFY_CHANGE_FILE_NAME |
			FILE_NOTIFY_CHANGE_DIR_NAME |
			FILE_NOTIFY_CHANGE_ATTRIBUTES |
			FILE_NOTIFY_CHANGE_SIZE |
			FILE_NOTIFY_CHANGE_LAST_WRITE |
			FILE_NOTIFY_CHANGE_LAST_ACCESS |
			FILE_NOTIFY_CHANGE_CREATION |
			FILE_NOTIFY_CHANGE_SECURITY);

		if (handles == INVALID_HANDLE_VALUE)
		{
			print_error("FindFirstChangeNotification function failed.\n");
			ExitProcess(GetLastError());
		}
	}

	for (var i = 0;; i++)
	{
		print("Waiting for change..");
		var result = WaitForMultipleObjects(arguments.watch_count, handles, false, INFINITE);
		var index = result - WAIT_OBJECT_0;
		if (index >= 0 && index < arguments.watch_count)
		{
			parse(&arguments, bufferpool, tokenpool, nodepool, attributepool, filepool, file_capacity);
			if (FindNextChangeNotification(handles[index]) == FALSE)
			{
				print("Change detected in: %s", arguments.watch_directories[index]);
				print_error("FindFirstChangeNotification function failed.\n");
				ExitProcess(GetLastError());
			}
		}

		var abandoned_index = result - WAIT_ABANDONED_0;
		if (abandoned_index >= 0 && abandoned_index < arguments.watch_count)
		{
			print("Watch handle abandoned: %s. Not supported! Exiting..", arguments.watch_directories[index]);
			return 1;
		}

		if (result == WAIT_TIMEOUT)
		{
			print_error("No changes in the timeout period.");
			break;
		}

		if (result == WAIT_FAILED)
		{
			print_error("WaitForMultipleObjects has failed.\n");
			ExitProcess(GetLastError());
		}
	}
	return 0;
}
